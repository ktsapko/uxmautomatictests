package com.smartbear.uxm.tests.configuration;


/**
 * 
 * List of configuration properties and their default values.
 * 
 * @author jmorante
 *
 */
public enum ConfigurationProperties {

	/*
	 * Test configuration options
	 */
	REQUESTS_TIME_OUT("test.requests.time.out", "30"), //Seconds
	DECOMPILATION_TIME_OUT("test.decompilation.time.out", "30"), //Seconds
	EXTENDED_TIMELINE_TIME_OUT("test.extendedtimeline.time.out", "5"), //Seconds
	
	/*
	 * UXM General properties
	 */
	UXM_URL("uxm.url", "https://uxm.qac.alertsite.com/ui/#/"),
	UXM_USER("uxm.user", "javier.morante@lucierna.com"),
	UXM_PASSWORD("uxm.password", "lucierna"),
	
	/*****************************
	 * JAVA 6 Transactions Tests *
	 *****************************/
	/*
	 * Slow transaction test
	 */
	SLOW_TRANSACTION_AGENT("test.slowtransaction.agent", "http://127.0.0.1:9090"),
	SLOW_TRANSACTION_URL("test.slowtransaction.url", "/java6AgentTestApplication/RealSlowTransactionServlet"),
	SLOW_TRANSACTION_ASSERT_PATTERN("test.slowtransaction.assert.pattern", "OK!"),
	SLOW_TRANSACTION_DECOMPILATION_PATTERN("test.slowtransaction.decompilation.pattern", "RealSlowTransactionServlet - Decompiled by JODE"),
	SLOW_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED("test.slowtransaction.boomerang.injection.check.enabled", "true"),
	
	/*
	 * Unhandled exception test
	 */
	UNHANDLED_EXCEPTION_TRANSACTION_AGENT("test.unhandledexceptiontransaction.agent", "http://127.0.0.1:9090"),
	UNHANDLED_EXCEPTION_TRANSACTION_URL("test.unhandledexceptiontransaction.url", "/java6AgentTestApplication/UnhandledExceptionServlet"),
	UNHANDLED_EXCEPTION_TRANSACTION_ASSERT_PATTERN("test.unhandledexceptiontransaction.assert.pattern", "Unhandled Exception!"),
	UNHANDLED_EXCEPTION_TRANSACTION_DECOMPILATION_PATTERN("test.unhandledexceptiontransaction.decompilation.pattern", "UnhandledExceptionServlet - Decompiled by JODE"),
	UNHANDLED_EXCEPTION_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED("test.unhandledexceptiontransaction.boomerang.injection.check.enabled", "false"),
	
	/*
	 * Slow DB Query transaction test
	 */
	SLOW_DB_QUERY_TRANSACTION_AGENT("test.slowdbquerytransaction.agent", "http://127.0.0.1:9090"),
	SLOW_DB_QUERY_TRANSACTION_URL("test.slowdbquerytransaction.url", "/java6AgentTestApplication/SlowDBQueryTransactionServlet"),
	SLOW_DB_QUERY_TRANSACTION_ASSERT_PATTERN("test.slowdbquerytransaction.assert.pattern", "Result: OK!"),
	SLOW_DB_QUERY_TRANSACTION_DECOMPILATION_PATTERN("test.slowdbquerytransaction.decompilation.pattern", "select_type=SIMPLE"),
	SLOW_DB_QUERY_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED("test.slowdbquerytransaction.boomerang.injection.check.enabled", "true"),
	
	
	
	/*****************************
	 * JAVA 7 Transactions Tests *
	 *****************************/
	/*
	 * Slow transaction test
	 */
	MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_AGENT("test.multipleexcpetionscatch.agent", "http://127.0.0.1:9090"),
	MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_URL("test.multipleexcpetionscatch.url", "/java7AgentTestApplication/MultipleExceptionsInCatchTransactionServlet"),
	MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_ASSERT_PATTERN("test.multipleexcpetionscatch.assert.pattern", "Answer OK!"),
	MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_DECOMPILATION_PATTERN("test.multipleexcpetionscatch.decompilation.pattern", "MultipleExceptionsInCatchTransactionServlet - Decompiled by JODE"),
	MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED("test.multipleexcpetionscatch.boomerang.injection.check.enabled", "true"),
	
	/*
	 * Unhandled exception test
	 */
	SWITCH_OF_STRING_TRANSACTION_AGENT("test.switchofstringtransaction.agent", "http://127.0.0.1:9090"),
	SWITCH_OF_STRING_TRANSACTION_URL("test.switchofstringtransaction.url", "/java7AgentTestApplication/SwitchOfStringServlet"),
	SWITCH_OF_STRING_TRANSACTION_ASSERT_PATTERN("test.switchofstringtransaction.assert.pattern", "Working OK!!"),
	SWITCH_OF_STRING_TRANSACTION_DECOMPILATION_PATTERN("test.switchofstringtransaction.decompilation.pattern", "SwitchOfStringServlet - Decompiled by JODE"),
	SWITCH_OF_STRING_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED("test.switchofstringtransaction.boomerang.injection.check.enabled", "true"),
	;
	
	
	
	private String propertyName;
	private String defaultValue;
	
	ConfigurationProperties(String propertyName, String defaultValue) {
		this.propertyName = propertyName;
		this.defaultValue = defaultValue;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	@Override
	public String toString() {
		return getPropertyName();
	}	
}
