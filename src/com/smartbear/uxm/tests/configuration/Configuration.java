package com.smartbear.uxm.tests.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Configuration options for JUnit tests of REAL.
 * 
 * This static class will try to recover the configuration options from "tests.properties" file,
 * if the file is not found, it will use the default values defined at "ConfigurationProperties" Enum.
 * 
 * The "tests.properties" should be located at "resources" folder or if it is located in a different
 * folder, it is necessary to define the JVM option "-Dtests.configuration.folder=" in order to indicate
 * the root folder of the configuratio file.
 * 
 * 
 * @author jmorante
 *
 */
public class Configuration {
	
	//JVM property to define the configuration folder.
	private static final String CONFIGURATION_FOLDER_PROPERTY = "tests.configuration.folder";
	
	//Default configuration folder (used when the configuration folder property is not defined).
	private static final String DEFAULT_CONFIGURATION_FOLDER = "resources";
	
	//Configuration file name.
	private static final String CONFIGURATION_FILE = "tests.properties";

	//List of properties.
	private static Properties configuration;
	
	/**
	 * This method will load the configuration properties required for the tests:
	 * 
	 * 1) If the JVM property "-Dtests.configuration.folder" is defined it will try to recover
	 * the configuration from the tests.properties file defined at that folder.
	 * 
	 * 2) If that JVM property is not defined, it will try to recover the configuration from
	 * "resources" folder.
	 * 
	 * 3) Finally, if no configuration file is found, it will leave the configuration properties list
	 * empty. So it will use the default values defined at ConfigurationProperties.java
	 */
	private static void init() {
		configuration = new Properties();
		
		String configFile = getConfigFolder() + File.separatorChar + CONFIGURATION_FILE;
		
		File file = null;
		FileInputStream fis = null;
		try {
			file = new File(configFile);
			if (!file.exists()) {
				System.err.println("FATAL - Cannot find configuration file: " + configFile);
				return;
			} else {
				fis = new FileInputStream(file);
				configuration.load(fis);
			}
		} catch (Throwable th) {
			System.err.println("FATAL - Cannot read configuration file (" + configFile + "): "+th);
			th.printStackTrace();
		} finally {
			if ( fis != null ) { 
				try {
					fis.close();
				} catch (Throwable th ) { th.printStackTrace(); }
			}
		}
	}
	
	/**
	 * Recovers the String value of a property.
	 * 
	 * If the property is not defined, it returns the default value defined at ConfigurationProperties.java.
	 * 
	 * @param property
	 * @return
	 */
	public static String getStringProperty(ConfigurationProperties property) {
		return recoverProperty(property.getPropertyName(), property.getDefaultValue());
	}
	
	/**
	 * Recovers the integer value of a property.
	 * 
	 * If the property is not defined, it returns the default value defined at ConfigurationProperties.java.
	 * 
	 * If the property is defined but is is not an integer, it will return 0.
	 * 
	 * @param property
	 * @return
	 */
	public static int getIntProperty(ConfigurationProperties property) {
		return parseInteger(recoverProperty(property.getPropertyName(), property.getDefaultValue()));
	}
	
	/**
	 * Recovers the boolean value of a property.
	 * 
	 * If the property is not defined, it returns the default value defined at ConfigurationProperties.java.
	 * 
	 * If the property is defined but is is not "true", it will return "false".
	 * 
	 * @param property
	 * @return
	 */
	public static boolean getBooleanProperty(ConfigurationProperties property) {
		return recoverProperty(property.getPropertyName(), property.getDefaultValue()).equalsIgnoreCase("true");
	}
	
	/**
	 * Tries to recover the property from the loaded configuration, if the property is not found,
	 * it returns the default value.
	 * 
	 * @param propertyName
	 * @param defaultValue
	 * @return
	 */
	protected static String recoverProperty(String propertyName, String defaultValue) {
		if ( configuration == null ) {
			init();
		}
		
		String ret = configuration.getProperty(propertyName, defaultValue);
		if (ret != null) {
			ret = ret.trim();
		}
		return ret;
	}
	
	/**
	 * Returns the configuration folder:
	 * 
	 * 1) First it tries to recover it from "-Dtests.configuration.folder" JVM option.
	 * 
	 * 2) If the JVM option is not defined it returns the default value ("resources" folder).
	 * 
	 * @return
	 */
	protected static String getConfigFolder() {
		String configFolder = System.getProperty(CONFIGURATION_FOLDER_PROPERTY);
		if (configFolder != null) {
			while (configFolder.endsWith(File.separator) || configFolder.endsWith("/")) {
				configFolder = configFolder.substring(0, configFolder.length() - 1);
			}
		} else {
			configFolder = DEFAULT_CONFIGURATION_FOLDER;
		}
		return configFolder;
	}
	
	/**
	 * Tries to parse the string to integer.
	 * 
	 * If it not possible it will return 0.
	 * 
	 * @param value
	 * @return
	 */
	private static int parseInteger(String value) {
		int id = 0;
		try {
			if ( value != null && ! value.equals("") ) {
				id = Integer.parseInt(value);
			}
		} catch (NumberFormatException th) {
			System.err.println("NumberFormatException for input string: " + value + " . Detail " + th);
			th.printStackTrace();
		}
		return id;
	}
}
