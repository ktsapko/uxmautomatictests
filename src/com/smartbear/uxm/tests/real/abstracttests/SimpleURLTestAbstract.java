package com.smartbear.uxm.tests.real.abstracttests;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;

/**
 * Simple test that calls to an URL and search a pattern.
 * 
 * Also checks if the TeX JavaScript has been injected
 * 
 * All the classes that extends this abstract class
 * have to call to the constructor" in order to define 
 * all the properties required for executing the test.
 * 
 * @author jmorante
 *
 */
public abstract class SimpleURLTestAbstract {
  
	protected WebDriver driver;
	protected StringBuffer verificationErrors = new StringBuffer();
	protected String url;
	protected String assertionPattern;
	protected boolean checkBoomerangInjection;
  
	public SimpleURLTestAbstract(String url, String assertionPattern, boolean checkBoomerangInjection) {
		super();
		this.url = url;
		this.assertionPattern = assertionPattern;
		this.checkBoomerangInjection = checkBoomerangInjection;
	}
	
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(
		    		Configuration.getIntProperty(ConfigurationProperties.REQUESTS_TIME_OUT), 
		    		TimeUnit.SECONDS);
	}

	@Test
	public void testURL() throws Exception {
    
		/*
		 * 1) Request URL
		 */
		driver.get(url);
		
		/*
		 * 2) Check content pattern
		 */
		String pageSource = driver.getPageSource();
		if ( ! pageSource.contains( assertionPattern ) ) {
			verificationErrors.append("ERROR: Transaction \""+ url + "\" has failed!! Pattern \""+assertionPattern+"\" not found!!\n\n");
		}
    
		/*
		 * 3) Check that Boomerang has been injected
		 */
		if ( this.checkBoomerangInjection && 
			  ! ( pageSource.contains( "boomerang" ) && pageSource.contains( "BOOMR.init" ) && pageSource.contains( "antorcha-userexperience" ) ) 
			) 
		{
			verificationErrors.append("ERROR: Boomerang is not present in \""+ url + "\"!!");
		}
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

}
