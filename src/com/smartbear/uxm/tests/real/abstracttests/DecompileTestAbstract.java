package com.smartbear.uxm.tests.real.abstracttests;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;

/**
 * 
 * Abstract test that defines a generic navigation in REAL 
 * in order to execute a decompilation or explain query for 
 * the slowest method of a transaction.
 * 
 * Once the decompilation or explain query is requested it waits for some 
 * seconds (ConfigurationProperties.DECOMPILATION_TIME_OUT).
 * 
 * Then it will search the pattern defined at "decompilationString" property. 
 * 
 * All the classes that extends this Abstract class has to call to the default
 * constructor in order to define all the properties required for the test.
 *  
 *  
 * @author jmorante
 *
 */
public abstract class DecompileTestAbstract {
  
  private static final int TIME_OUT_FOR_DECOMPILATION = 1000*Configuration.getIntProperty(ConfigurationProperties.DECOMPILATION_TIME_OUT);
  
  private static final int TIME_OUT_FOR_EXTENDED_TIMELINE = 1000*Configuration.getIntProperty(ConfigurationProperties.EXTENDED_TIMELINE_TIME_OUT);
  
  
  protected WebDriver driver;
  
  //Base URL of UXM
  protected String baseUrl;
  //Login email for UXM
  protected String email;
  //Password for UXM
  protected String password;
  //Assertion pattern that will be searched after calling to decompilation or explain query of slowest method of the transaction
  protected String decompilationString;
  //If enabled it will check that the End user experience information is present in the expadend time line.
  protected boolean checkEndUserExperience;
  
  private StringBuffer verificationErrors = new StringBuffer();

  public DecompileTestAbstract(String baseUrl, String email, String password, String decompilationString, boolean checkEndUserExperience) {
	  super();
	  this.baseUrl = baseUrl;
	  this.email = email;
	  this.password = password;
	  this.decompilationString = decompilationString;
	  this.checkEndUserExperience = checkEndUserExperience;
  }
  
  /**
   * This method has to initialize the "driver" and define the values
   * protected properties: baseUrl, email, password and decompilationString.
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
	driver = new FirefoxDriver();
	driver.manage().timeouts().implicitlyWait(
	    		Configuration.getIntProperty(ConfigurationProperties.REQUESTS_TIME_OUT), 
	    		TimeUnit.SECONDS);
  }

  @Test
  public void decompileSlowestMethod() throws Exception {
    driver.get(baseUrl + "/ui/#/");

    /*
     * 1) Login
     */
    driver.findElement(By.id("id_email")).clear();
    driver.findElement(By.id("id_email")).sendKeys(this.email);
    driver.findElement(By.id("id_password")).clear();
    driver.findElement(By.id("id_password")).sendKeys(this.password);
    driver.findElement(By.xpath("//input[@value='Sign In']")).click();
    
    /*
     * 2) Access to REAL
     */
    driver.findElement(By.partialLinkText("Real")).click();
    
    /*
     * 3) Select "Issues" menu
     */
    driver.findElement(By.partialLinkText("Issues")).click();
    
    /*
     * 4) Select "Occurrences" menu
     */
    driver.findElement(By.partialLinkText("Occurrences")).click();
    
    /*
     * 5) Select the first "Occurrence"
     */
    driver.findElement(By.xpath("//div[@id='root_cause_defects_list']/div[2]/div/div[2]/div[2]/div/table/tbody/tr/td[11]/span/i")).click();
    
    /*
     * 6) Check that End User Experience information is present
     */
    if ( this.checkEndUserExperience ) {
        //6.1 - Open the expanded timeline
    	driver.findElement(By.xpath("//div[@id='root_cause_transaction_execution_timeline']/div[2]/div/div[2]/div/div/button")).click();
        
    	//6.2 - Check that the user experience information is present.
    	 //Sleep in order to wait for extended timeline result.
        try{
        	Thread.sleep(TIME_OUT_FOR_EXTENDED_TIMELINE);
        } catch (Throwable th){ /*Do nothing*/ }
    	String sourceCode = driver.getPageSource();
    	if ( ! sourceCode.contains("End User Experience Layer") ) {
    		this.verificationErrors.append("ERROR: End User Experience Layer information not found!!! \n\n");
    	}
    	
    	//6.3 - Close the expanded timeline
    	driver.findElement(By.cssSelector("div.modal-header > button.close")).click();
    }
    
    
    /*
     * 6) Select the first method to decompile
     */
    driver.findElement(By.xpath("//div[@id='root_cause_transaction_slowest_modules']/div[2]/div/div[2]/div[2]/div/table/tbody/tr/td[6]/span/span")).click();
    
    /*
     * 7) Check that the decompilation has worked fine
     */
    //Sleep in order to wait for decompilation result.
    try{
    	Thread.sleep(TIME_OUT_FOR_DECOMPILATION);
    } catch (Throwable th){ /*Do nothing*/ }
    //Recover page source code and check that the expected code is pressent.
    String pageSource = driver.getPageSource();
    if ( !pageSource.contains(this.decompilationString) )
    {
    	this.verificationErrors.append("ERROR: Decompilation string not found!!! [\""+this.decompilationString+"\"]");
    } 
    
    /*
     * 8) Close the decompilation window
     */
    driver.findElement(By.cssSelector("div.modal-header > button.close")).click();
    
    /*
     * 9) Log out
     */
    driver.findElement(By.cssSelector("i.fa.fa-chevron-down")).click();
    driver.findElement(By.linkText("Log out")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

}
