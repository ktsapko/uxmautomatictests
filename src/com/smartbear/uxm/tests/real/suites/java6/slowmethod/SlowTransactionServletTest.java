package com.smartbear.uxm.tests.real.suites.java6.slowmethod;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;
import com.smartbear.uxm.tests.real.abstracttests.SimpleURLTestAbstract;

public class SlowTransactionServletTest extends SimpleURLTestAbstract {

	public SlowTransactionServletTest() {
		super(
			Configuration.getStringProperty(ConfigurationProperties.SLOW_TRANSACTION_AGENT) + 
	    		Configuration.getStringProperty(ConfigurationProperties.SLOW_TRANSACTION_URL),
	    	Configuration.getStringProperty(ConfigurationProperties.SLOW_TRANSACTION_ASSERT_PATTERN),
	    	Configuration.getBooleanProperty(ConfigurationProperties.SLOW_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED)
			);
	}
}
