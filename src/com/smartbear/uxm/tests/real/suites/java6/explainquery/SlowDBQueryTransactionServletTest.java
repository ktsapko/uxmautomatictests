package com.smartbear.uxm.tests.real.suites.java6.explainquery;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;
import com.smartbear.uxm.tests.real.abstracttests.SimpleURLTestAbstract;


/**
 * Calls to an URL with an slow DB query. 
 * 
 * The slowest method of the transaction is the DB query.
 * 
 * @author jmorante
 *
 */
public class SlowDBQueryTransactionServletTest extends SimpleURLTestAbstract {

	public SlowDBQueryTransactionServletTest() {
		super(
			Configuration.getStringProperty(ConfigurationProperties.SLOW_DB_QUERY_TRANSACTION_AGENT) + 
	    		Configuration.getStringProperty(ConfigurationProperties.SLOW_DB_QUERY_TRANSACTION_URL),
	    	Configuration.getStringProperty(ConfigurationProperties.SLOW_DB_QUERY_TRANSACTION_ASSERT_PATTERN),
	    	Configuration.getBooleanProperty(ConfigurationProperties.SLOW_DB_QUERY_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED));
	}
	
}
