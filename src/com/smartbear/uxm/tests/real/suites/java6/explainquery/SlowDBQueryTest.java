package com.smartbear.uxm.tests.real.suites.java6.explainquery;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;
import com.smartbear.uxm.tests.real.abstracttests.DecompileTestAbstract;

/**
 * Checks that the last occurrence is a transaction
 * with a query as the slowest method.
 * 
 * Also checks that the result of explain query command
 *  meets the expected result.
 * 
 * @author jmorante
 *
 */
public class SlowDBQueryTest extends DecompileTestAbstract {

	public SlowDBQueryTest() {
		super(Configuration.getStringProperty(ConfigurationProperties.UXM_URL), 
				Configuration.getStringProperty(ConfigurationProperties.UXM_USER), 
				Configuration.getStringProperty(ConfigurationProperties.UXM_PASSWORD), 
				Configuration.getStringProperty(ConfigurationProperties.SLOW_DB_QUERY_TRANSACTION_DECOMPILATION_PATTERN),
				Configuration.getBooleanProperty(ConfigurationProperties.SLOW_DB_QUERY_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED));
	}

}
