package com.smartbear.uxm.tests.real.suites.java6.unhandledexception;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;
import com.smartbear.uxm.tests.real.abstracttests.SimpleURLTestAbstract;

public class UnhandledExceptionServletTest extends SimpleURLTestAbstract {

	
	public UnhandledExceptionServletTest() {
		super(
			Configuration.getStringProperty(ConfigurationProperties.UNHANDLED_EXCEPTION_TRANSACTION_AGENT) + 
	    		Configuration.getStringProperty(ConfigurationProperties.UNHANDLED_EXCEPTION_TRANSACTION_URL),
	    	Configuration.getStringProperty(ConfigurationProperties.UNHANDLED_EXCEPTION_TRANSACTION_ASSERT_PATTERN),
	    	Configuration.getBooleanProperty(ConfigurationProperties.UNHANDLED_EXCEPTION_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED)
			);
	}
	
}
