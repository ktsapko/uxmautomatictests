package com.smartbear.uxm.tests.real.suites.java6.explainquery;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Suite for testing the explain query feature.
 * 
 * 1) Calls to a transaction with an slow query as its slowest method.
 * 
 * 2) Executes in UXM the explain query feature and checks that the result includes the expected pattern.
 * 
 * @author jmorante
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ SlowDBQueryTransactionServletTest.class, SlowDBQueryTest.class })
public class SlowDBQueryTransactionSuite {

}