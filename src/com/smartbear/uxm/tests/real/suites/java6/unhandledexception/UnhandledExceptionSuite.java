package com.smartbear.uxm.tests.real.suites.java6.unhandledexception;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ UnhandledExceptionServletTest.class, UnhandledExceptionTransactionDecompileTest.class })
public class UnhandledExceptionSuite {

}