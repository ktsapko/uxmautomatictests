package com.smartbear.uxm.tests.real.suites.java6.slowmethod;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;
import com.smartbear.uxm.tests.real.abstracttests.DecompileTestAbstract;

public class SlowMethodDecompileTest extends DecompileTestAbstract {

	public SlowMethodDecompileTest() {
		super(	Configuration.getStringProperty(ConfigurationProperties.UXM_URL), 
				Configuration.getStringProperty(ConfigurationProperties.UXM_USER), 
				Configuration.getStringProperty(ConfigurationProperties.UXM_PASSWORD), 
				Configuration.getStringProperty(ConfigurationProperties.SLOW_TRANSACTION_DECOMPILATION_PATTERN),
				Configuration.getBooleanProperty(ConfigurationProperties.SLOW_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED));
	}

}
