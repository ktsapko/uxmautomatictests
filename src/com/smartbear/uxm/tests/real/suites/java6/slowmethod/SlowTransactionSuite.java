package com.smartbear.uxm.tests.real.suites.java6.slowmethod;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SlowTransactionServletTest.class, SlowMethodDecompileTest.class })
public class SlowTransactionSuite {

}