package com.smartbear.uxm.tests.real.suites.java6.unhandledexception;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;
import com.smartbear.uxm.tests.real.abstracttests.DecompileTestAbstract;

public class UnhandledExceptionTransactionDecompileTest extends DecompileTestAbstract {

	public UnhandledExceptionTransactionDecompileTest() {
		super(Configuration.getStringProperty(ConfigurationProperties.UXM_URL), 
				Configuration.getStringProperty(ConfigurationProperties.UXM_USER), 
				Configuration.getStringProperty(ConfigurationProperties.UXM_PASSWORD), 
				Configuration.getStringProperty(ConfigurationProperties.UNHANDLED_EXCEPTION_TRANSACTION_DECOMPILATION_PATTERN),
				Configuration.getBooleanProperty(ConfigurationProperties.UNHANDLED_EXCEPTION_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED));
	}

}
