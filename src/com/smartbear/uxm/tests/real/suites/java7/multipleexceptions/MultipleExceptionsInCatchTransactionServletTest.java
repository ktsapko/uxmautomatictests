package com.smartbear.uxm.tests.real.suites.java7.multipleexceptions;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;
import com.smartbear.uxm.tests.real.abstracttests.SimpleURLTestAbstract;

public class MultipleExceptionsInCatchTransactionServletTest extends SimpleURLTestAbstract {

	public MultipleExceptionsInCatchTransactionServletTest() {
		super(
			Configuration.getStringProperty(ConfigurationProperties.MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_AGENT) + 
	    		Configuration.getStringProperty(ConfigurationProperties.MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_URL),
	    	Configuration.getStringProperty(ConfigurationProperties.MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_ASSERT_PATTERN),
	    	Configuration.getBooleanProperty(ConfigurationProperties.MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED)
			);
	}
}
