package com.smartbear.uxm.tests.real.suites.java7.switchofstring;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ SwitchOfStringTransactionServletTest.class, SwitchOfStringTransactionDecompileTest.class })
public class SwitchOfStringTransactionSuite {

}