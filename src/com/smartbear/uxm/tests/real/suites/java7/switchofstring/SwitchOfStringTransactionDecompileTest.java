package com.smartbear.uxm.tests.real.suites.java7.switchofstring;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;
import com.smartbear.uxm.tests.real.abstracttests.DecompileTestAbstract;

public class SwitchOfStringTransactionDecompileTest extends DecompileTestAbstract {

	public SwitchOfStringTransactionDecompileTest() {
		super(Configuration.getStringProperty(ConfigurationProperties.UXM_URL), 
				Configuration.getStringProperty(ConfigurationProperties.UXM_USER), 
				Configuration.getStringProperty(ConfigurationProperties.UXM_PASSWORD), 
				Configuration.getStringProperty(ConfigurationProperties.SWITCH_OF_STRING_TRANSACTION_DECOMPILATION_PATTERN),
				Configuration.getBooleanProperty(ConfigurationProperties.SWITCH_OF_STRING_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED)
				);
	}

}
