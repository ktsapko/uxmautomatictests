package com.smartbear.uxm.tests.real.suites.java7.multipleexceptions;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MultipleExceptionsInCatchTransactionServletTest.class, MultipleExceptionsInCatchDecompileTest.class })
public class MultipleExceptionsInCatchSuite {

}