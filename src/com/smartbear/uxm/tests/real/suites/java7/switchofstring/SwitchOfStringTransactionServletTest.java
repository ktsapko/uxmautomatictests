package com.smartbear.uxm.tests.real.suites.java7.switchofstring;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;
import com.smartbear.uxm.tests.real.abstracttests.SimpleURLTestAbstract;

public class SwitchOfStringTransactionServletTest extends SimpleURLTestAbstract {

	public SwitchOfStringTransactionServletTest() {
		super(
			Configuration.getStringProperty(ConfigurationProperties.SWITCH_OF_STRING_TRANSACTION_AGENT) + 
	    		Configuration.getStringProperty(ConfigurationProperties.SWITCH_OF_STRING_TRANSACTION_URL),
	    	Configuration.getStringProperty(ConfigurationProperties.SWITCH_OF_STRING_TRANSACTION_ASSERT_PATTERN),
	    	Configuration.getBooleanProperty(ConfigurationProperties.SWITCH_OF_STRING_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED)
			);
	}
	
}
