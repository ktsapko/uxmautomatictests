package com.smartbear.uxm.tests.real.suites.java7.multipleexceptions;

import com.smartbear.uxm.tests.configuration.Configuration;
import com.smartbear.uxm.tests.configuration.ConfigurationProperties;
import com.smartbear.uxm.tests.real.abstracttests.DecompileTestAbstract;

public class MultipleExceptionsInCatchDecompileTest extends DecompileTestAbstract {

	public MultipleExceptionsInCatchDecompileTest() {
		super(	Configuration.getStringProperty(ConfigurationProperties.UXM_URL), 
				Configuration.getStringProperty(ConfigurationProperties.UXM_USER), 
				Configuration.getStringProperty(ConfigurationProperties.UXM_PASSWORD), 
				Configuration.getStringProperty(ConfigurationProperties.MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_DECOMPILATION_PATTERN),
				Configuration.getBooleanProperty(ConfigurationProperties.MULTIPLE_EXCEPTIONS_CATCH_TRANSACTION_BOOMERANG_INJECTION_CHECK_ENABLED));
	}
	
}
