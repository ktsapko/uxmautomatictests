package com.smartbear.uxm.tests.real;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.smartbear.uxm.tests.real.suites.java6.explainquery.SlowDBQueryTransactionSuite;
import com.smartbear.uxm.tests.real.suites.java6.slowmethod.SlowTransactionSuite;
import com.smartbear.uxm.tests.real.suites.java6.unhandledexception.UnhandledExceptionSuite;
import com.smartbear.uxm.tests.real.suites.java7.multipleexceptions.MultipleExceptionsInCatchSuite;
import com.smartbear.uxm.tests.real.suites.java7.switchofstring.SwitchOfStringTransactionSuite;

/**
 * Junit Suite that includes all the tests defined in the project
 * 
 * 
 * @author jmorante
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ 
	SlowTransactionSuite.class, 
	UnhandledExceptionSuite.class,
	SlowDBQueryTransactionSuite.class,
	MultipleExceptionsInCatchSuite.class,
	SwitchOfStringTransactionSuite.class
	})
public class REALTestsSuite {

}
