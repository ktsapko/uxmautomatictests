package com.smartbear.uxm.tests.common;

import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GetIDPTokenTest {
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

  
	 
	//FTR
	private static final String BASE_URL = "https://accounts.ftr.smartbear.com";
	private static final String USER = "javier.morante@smartbear.com";
	private static final String PASSWORD = "password[";
	
	//QA
//	private static final String BASE_URL = "https://accounts.qac.core.smartbear.com";
//	private static final String USER = "javier.morante@lucierna.com";
//	private static final String PASSWORD = "lucierna"; 
		
  
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = BASE_URL;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testGetIDPToken() throws Exception {
		driver.get(baseUrl + "/auth/openid-connect/get-token-ui");
		driver.findElement(By.name("user_email")).clear();
		driver.findElement(By.name("user_email")).sendKeys(USER);
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(PASSWORD);
		driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();

		
		WebElement result = driver.findElement(By.cssSelector("textarea"));
		
		if ( result != null && result.getText() != null 
				&& result.getText().contains("Token") ) {
			System.out.println("[TOKEN="+getTokenFromGetIDPTokenResponse(result.getText())+"]");
		} else {
			fail("Authentication failed!!!!");
		}
	}
	
	private String getTokenFromGetIDPTokenResponse(String response) {
		String token = null;
		
		String[] responseSplit = response.split("Token:");
		
		if ( responseSplit.length == 2 ) {
			token = responseSplit[1].trim();
		}
		return token;
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
